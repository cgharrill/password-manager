from configparser import ConfigParser
from os.path import exists

config = ConfigParser()
path_to_config = 'src/config.ini'
preference_selections = ["Use Uppercase Letters?",
                        "Use Lowercase Letters?", 
                        "Use Special Characters?", 
                        "Use Numbers?", 
                        "Password Length (Number of Characters)"]
config_selections = ["Use_Uppercase",
                    "Use_Lowercase",
                    "Use_Special",
                    "Use_Numbers",
                    "Password_Length"]
input_options = ("(Y/N)")
preference_list = {}


def generate_default_configs():
    config.read(path_to_config)
    config.add_section('main')
    config.set('main', config_selections[0], 'y')
    config.set('main', config_selections[1], 'y')
    config.set('main', config_selections[2], 'y')
    config.set('main', config_selections[3], 'y')
    config.set('main', config_selections[4], '15')

    with open(path_to_config, 'w') as f:
        config.write(f)

def check_config_existence():
    config_file_exists = exists(path_to_config)
    if not config_file_exists:
        generate_default_configs()

def write_user_config(setting, setting_value):
    config.read(path_to_config)
    config.set('main', setting, setting_value)

    with open(path_to_config, 'w') as f:
        config.write(f)

def user_config_menu():
#TODO: allow enter to keep old value
    for i in range (0,len(preference_selections)):
        while True:
            user_input = input(f'{preference_selections[i]} {input_options}: ').lower().strip()
            if config_selections[i] == "Password_Length":
                if user_input.isdigit():
                    preference_list.update({config_selections[i]: user_input})
                    break
                else:
                    print("Invalid Input, try again.")
                    continue
            else:
                if user_input == 'y' or user_input == 'n':
                    preference_list.update({config_selections[i]: user_input})
                    break
                else:
                    print("Invalid input, try again.")

    input_test = list(preference_list.values())[0:4]

    if input_test == ['n', 'n', 'n', 'n']:
        print("At least one character type must be selected.")
        user_config_menu()
        
    print('') #TODO: for some reason a \n adds two lines, need to figure out why.

    for x in range (0, len(preference_list)):
        write_user_config(config_selections[x], preference_list[config_selections[x]])

def read_user_prefs():
    config.read(path_to_config)
    prefs = {}
    for key in config['main']:
        prefs.update({key: config['main'][key]})

    return prefs