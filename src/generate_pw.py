from configparser import ConfigParser
from configuration import read_user_prefs
from random import SystemRandom

system_random = SystemRandom()

config = ConfigParser()
upper_case_letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
lower_case_letters = 'abcdefghijklmnopqrstuvwxyz'
numbers = '0123456789'
special_characters = '!@#$%^&*'

def generate_password():
    read_prefs = read_user_prefs()
    password_length = int(read_prefs['password_length'])
    character_list = build_character_list(read_prefs)
    password = ""
    for i in range (0,password_length):
        password = password + character_list[system_random.randint(0,len(character_list)-1)]

    print(f'\nYour password is: {password}\n')

def build_character_list(prefs):
    characters = ""
    if prefs['use_uppercase'] == 'y':
        characters = characters + upper_case_letters
    if prefs['use_lowercase'] == 'y':
        characters = characters + lower_case_letters
    if prefs['use_numbers'] == 'y':
        characters = characters + numbers
    if prefs['use_special'] == 'y':
        characters = characters + special_characters
    return characters