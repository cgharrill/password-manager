from configuration import check_config_existence, user_config_menu
from generate_pw import generate_password
import colorama as c
import sys, os

def clear():
    os.system('cls' if os.name == 'nt' else 'clear')

def main():
    c.init()
    clear()
    print("\033[38;5;6mPassword Generator\n\033[38;5;231m")
    while True:
        check_config_existence()

        #TODO no input validation here!
        #menu_user_selection = input("Select an option:\n1: Generate Password\n2: Update Preferences\n3: Quit\n\n>>> ")
        print("Select an Option:")
        print("\033[38;5;6m1: Generate Password\033[38;5;231m")
        print("\033[38;5;6m2: Configure Preferences\033[38;5;231m")
        print("\033[38;5;2m3: Quit\033[38;5;231m")
        menu_user_selection = input(">>> ")
        try:
            menu_user_selection = int(menu_user_selection)
        except:
            print("\033[38;5;1mInvalid Input, try again.\033[38;5;231m")

        if menu_user_selection == 1: 
            generate_password()
        elif menu_user_selection == 2:
            user_config_menu()
        elif menu_user_selection == 3:
            sys.exit()

if __name__ == "__main__":
    main()